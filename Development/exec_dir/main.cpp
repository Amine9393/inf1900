/*
 * Nom: TP6 - P2
 * Copyright (C) 2018 - Amine Kamal (1718831) et Jean-Olivier Dalphond (1873653)
 * License http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * Description: Ceci est un exemple simple de programme 
 * Version: 1.1
 */

#define F_CPU 8000000UL //8 MHz
#include "Robot.h"
//#include "WallRenderer.h"

int main()
{
	Robot robot;
	//robot.print();
	while(1)
	{
		robot.run();
	}
}
