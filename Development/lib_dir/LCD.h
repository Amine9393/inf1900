/****************************************************
// Fichier : LCD.h
// Auteur: Jean-Olivier Dalphond (1873653) | Équipes 23-28
// Date de création: 12/11/18
// Description: Entête de la classe LCD
*****************************************************/

#ifndef LCD_H
#define LCD_H

#define F_CPU 8000000UL //8 MHz
#include <stdlib.h>
#include <avr/io.h>
	
#include "lcm_so1602dtr_m_fw.h"
#include "lcm_so1602dtr_m.h"
#include "customprocs.h"

class LCD
{
public:
	// pour afficher
	void displayDouble(double valeur);
	void displayLR(double L, double R);
	void displayWallDebug(double L, double R);
	void display(const char* text);
	void display(uint8_t text);
	void display(uint16_t text);
	void display(double valeur);
	void display(int text);
	void clear();
	
private:
	static LCM disp;
};
#endif
