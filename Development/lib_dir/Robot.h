/****************************************************
// Fichier : Robot.h
// Auteur: Amine Kamal (1718831) | Équipes 23-28
// Date de création: 30/10/18
// Description: Entête de la classe Robot
*****************************************************/

#ifndef ROBOT_H
#define ROBOT_H

#include "PortControl.h"
#include "Communication.h"

#include <avr/interrupt.h>
#include "Movement.h"
#include "WallRenderer.h"
#include "Light.h"
#include "sound.h"
#include "Utils.h"
#include "Interrupt.h"
#include "LCD.h"

enum class RState
{
	INIT,
	AQ,
	NAQ,
	ECHO,
	PRINT,
	END
};

class Robot
{

public:
	Robot();
	~Robot();

	void run();
	static void setTimer(uint16_t delay);
	static void timerFunction();
	static volatile bool WALL_DISPLAY_FINISHED;
	void print();

public:
	void debugMesure();
	void debugPrint();
	void debugWait();
	void init();
	void aq();
	void naq();
	void echo();
	
	void end();
	void initTimer();

	void gotoState(RState state);
	void toggleAcqSide();

	//Constants
	const char* NAME = "Disco Ball";
	const char INTERRUPT_PORT = 'B';
	const uint8_t INTERRUPT_PIN = 5; // Fil ROUGE SUR PB6 ET AUTRE ROUGE SUR LE COTE GAUCHE

	//Attributs
	RState currentState_;
	Direction turnDirection_;
	Direction acquisitionSide_;

	//Dependencies attributes
	Movement mvt_;
	WallRenderer wrd_;
	LCD lcd_;
};

#endif
