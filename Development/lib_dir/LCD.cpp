/****************************************************
// Fichier : LCD.cpp
// Auteur: Jean-Olivier Dalphond (1873653) | Équipes 23-28
// Date de création: 12/11/18
// Description: Implémentation de la classe LCD
*****************************************************/
#include "LCD.h"
#include <stdio.h>

// constructeur
LCM LCD::disp(&DDRC, &PORTC);

// méthode pour vider l'écran
void LCD::clear()
{
	disp.clear();
}

// méthode pour afficher un double appelée par la fonction générique display
// prend en paramètre un double
// NOTE: seulement 2 décimales de précision
void LCD::displayDouble(double valeur)
{
	int entier = valeur;
	valeur -= entier;
	int decimales = valeur*100;
	disp << entier << '.' << decimales;
}

// méthode pour afficher les valeurs de chaque capteur (fin du parcours)
// prend 2 doubles en paramètres, affiche sur 2 lignes
void LCD::displayLR(double L, double R)
{
	int Left = L;
	int Right = R;
	disp << "GAUCHE: " << Left << " cm" << "  " << "DROITE: " << Right << " cm";
}

// méthode pour afficher les valeurs de chaque capteur pendant le parcours
// prend 2 doubles en paramètres, affiche sur une seule ligne
void LCD::displayWallDebug(double L, double R)
{
	int Left = L;
	int Right = R;
	disp << "G: " << Left << "cm" << " " << "D: " << Right << "cm";
}

// méthodes pour afficher le nom du robot au début du parcours (ou autre chose)
// prend en paramètre un string, ou un uint8_t, ou un double, ou un int
void LCD::display(const char* text)
{
	disp << text;
}

void LCD::display(uint8_t text)
{
	disp << text;
}

void LCD::display(double valeur)
{
	int entier = valeur;
	valeur -= entier;
	int decimales = valeur*100;
	disp << entier << '.' << decimales;
}

void LCD::display(uint16_t text)
{
	disp << text;
}

void LCD::display(int text)
{
	disp << text;
}
