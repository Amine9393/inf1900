/*
 * Nom: WallSensorManager.h
 * Auteur: Christopher Sigouin-Bond - équipe 23-28
 * Date: 13 novembre 2018
 */
 
#ifndef WALL_SENSOR_MANAGER_H
#define WALL_SENSOR_MANAGER_H

#include "can.h"

#include <math.h>
#include <avr/io.h>

class WallSensorManager
{	
	private:
		can converter_; // Convertisseur analogique
		
		const uint8_t LEFT_SENSOR_PIN = 0x6; // Pin 1 et 2 du port A.
		const uint8_t RIGHT_SENSOR_PIN = 0x7;
		
		
		// Retourne la valeur numérique d'un capteur
		uint8_t getSensorData(const uint8_t pinNumber);
		
		// Retourne la valeur numérique du capteur gauche ou droit
		uint8_t getLeftSensorData();
		uint8_t getRightSensorData();
		
		// Calcul de la distance
		uint8_t calculateDistanceCm(const uint8_t data) const;
		
	public:
		// Retourne la distance entre le robot et un mur
		uint8_t getDistanceFromLeftWall();
		uint8_t getDistanceFromRightWall();	
};
#endif
	

