/****************************************************
* Fichier : PWM.cpp
* Auteur: Sébastien Zerbato (1573817) | Équipes 23-28
* Date de création: 23/10/18
* Description: Implémentation de la classe PWM
*
*****************************************************/

#include "PWM.h"

void PWM::ajustementPWM1 (uint8_t a1, uint8_t b1) {

    // mise à un des sorties OC1A et OC1B sur comparaison
    // réussie en mode PWM 8 bits, phase correcte
    // et valeur de TOP fixe à 0xFF (mode #1 de la table 17-6)   
    // page 177 de la description technique du ATmega324PA)
    cli();
   
    OCR1A =a1;
    OCR1B =b1;

    // division d'horloge par 8 - implique une frequence de PWM fixe

    TCCR1A |= (1<<COM1A1)  | (1<<COM1B1) | (1<<WGM10);

    TCCR1B |= (1<<CS11) ;

    TCCR1C = 0;
    sei();
}
void PWM::ajustementPWM2 ( uint8_t a2, uint8_t b2) {

    // mise à un des sorties OC2A et OC2B sur comparaison

    // réussie en mode PWM 8 bits, phase correcte

    // et valeur de TOP fixe à 0xFF (mode #1 de la table 17-6)   

    // page 177 de la description technique du ATmega324PA)


    cli();
    OCR2A =a2;
    OCR2B =b2;

    // division d'horloge par 8 - implique une frequence de PWM fixe

    TCCR2A |= (1<<COM2A1) +(1<<COM2B1) +(1<<WGM20);

    TCCR2B |= (1<<CS21) ;

    //TCCR2C = 0;
    sei();
}           
