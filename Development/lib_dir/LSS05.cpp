/****************************************************
// Fichier : LSS05.cpp
// Auteur: Samuel Charbonneau  | Équipes 23-28
// Date de création: 14/11/18
// Description: Gestion des capteurs du suiveur de ligne LSS05
*****************************************************/

#include "LSS05.h"

/*
 * Explication du branchement sur le port de votre choix
 *	2	4	6	8  VCC
 *	▓	▓	▓	o	▓
 * 
 *	▓	▓	▓	o	▓
 * 	1	3	5	7  GND
 * 
 * La flèche du connecteur à 6 fil doit être sur la pin 5
 * La flèche du connecteur à 2 fil doit être sur la pin G
 * ____________________	
 * |Utilisateur | Pin |
 * |------------|-----|
 * |capteur 1   | 1   |
 * |capteur 2   | 2   |
 * |capteur 3:  | 3   |
 * |capteur 4:  | 4   |
 * |capteur 5:  | 5   |
 * |calibrage:  | 6   |
 * |____________|_____|
 * 
*/


/** ----------------- Section Public ----------------- **/

/*
 * Constructeur qui prend en paramètre le port avec lequel on travail.
 * S'assurer de mettre la lettre du port en majuscule.
*/

LSS05::LSS05(){
	port_ = 'A';
	currentOrientation_ = Direction::CENTERED;
	calibrate();
}

/*
* Retourne l'orientation du robot.
* Si le robot détecte rien, son orientation ne changera pas.
* Il vérifie d'abord si est orienté vers la droite, ensuite vers la gauche sinon s'il est droit.
*/
Direction LSS05::getOrientation(){
	
	if(checkIRSensor(1) || checkIRSensor(2))
		currentOrientation_ =  Direction::RIGHT;
	else if(checkIRSensor(4)|| checkIRSensor(5))
		currentOrientation_ =  Direction::LEFT;
	else if(checkIRSensor(3))
		currentOrientation_ = Direction::CENTERED;
	return currentOrientation_;
}


/*
 * Vérifie juste si le robot est centré.
 * Je dois avoir cette fonction pour faire le changement de ligne.
 * Différente de getOrientation.
*/
bool LSS05::isCentered(){
	if(checkIRSensor(3))
		return true;
	else
	return false;
}



/* 
 * Vérifie si le robot détecte soit une ligne horizontale ou aucune ligne.
 * D'abord, il vérifie une ligne horizontale, ensuite s'il détecte rien sur tous ses capteurs.
 * Sinon, il retourne NONE qui veut dire qu'on est dans aucun des cas.
*/
detectAllSame LSS05::checkIfAllSensorDetectSame(){
	if(checkIRSensor(1) && checkIRSensor(2) && checkIRSensor(3) && checkIRSensor(4) && checkIRSensor(5))
		return detectAllSame::LINE;
	else if(!checkIRSensor(1) && !checkIRSensor(2) && !checkIRSensor(3) && !checkIRSensor(4) && !checkIRSensor(5) ){
		_delay_ms(50);
		if(!checkIRSensor(1) && !checkIRSensor(2) && !checkIRSensor(3) && !checkIRSensor(4) && !checkIRSensor(5) )
			return detectAllSame::NOTHING;
	}
	else
		return detectAllSame::NONE;
}



/*
 *  Vérifie si le robot détecte un coin selon le côté passé en paramètre 
 */
bool LSS05::detectCorner(Direction sideToCheck){
	if(sideToCheck==Direction::RIGHT){
		if(checkIRSensor(4) && checkIRSensor(5)){
			return true;
		}
	}
	else{
		if(checkIRSensor(2) && checkIRSensor(1)){
			return true;
		}
	}
	return false;
}


/*
 *  Vérifie si le robot détecte un coin avec que deux capteurs selon le côté passé en paramètre 
 */
bool LSS05::detectCornerByTwo(Direction sideToCheck){
	if(sideToCheck==Direction::RIGHT){
		if( checkIRSensor(4) && checkIRSensor(5)){
			return true;
		}
	}
	else{
		if( checkIRSensor(2) && checkIRSensor(1)){
			return true;
		}
	}
	return false;
}


bool LSS05::endOfLine(Direction sideToCheck){
	if(sideToCheck==Direction::RIGHT){
		if(checkIRSensor(5) ==false){
			return true;
		}
	}
	else{
		if(checkIRSensor(1) ==false){
			return true;
		}
	}
	return false;
	
}


/* 
 * Vérifie si au moins un capteur est actif.
 * Utile pour lorsqu'on va tweaker le robot pour savoir si nous sommes au bout d'une ligne.
*/
bool LSS05::atLeastOneSensorDetect(){
	
	if(checkIRSensor(1) || checkIRSensor(2) || checkIRSensor(3) || checkIRSensor(4) || checkIRSensor(5))
		return true;
	else
		return false;
	
}

/* 
 * Vérifie qul côté le capteur est actif.
 * Utile pour lorsqu'on va tweaker le robot pour savoir si nous sommes au bout d'une ligne et pour reTweaker du bon côté.
*/
Direction LSS05::wichSensorSide(){
	
	if(checkIRSensor(1) || checkIRSensor(2) ){
		return Direction::LEFT;
	}
	else if(  checkIRSensor(4) || checkIRSensor(5)){
		return Direction::RIGHT;
	}
	else{
		return Direction::CENTERED;
	}	
}




/*
 * Va calibrer les capteurs pour s'assurer de toujours une bonne référence malgré la luminosité ambiante qui peut changer
*/

void LSS05::calibrate(){
	

}


/** ----------------- Section Privé ----------------- **/

/* 
 * Vérifie l'état d'un des capteurs du suiveur de ligne.
 * sensorNumber contient le numéro du sensor à vérifier.
 * On soustrait 1 puisque le capteur est connecter à partir de 0.
 * Se référer au tableau décrivant le port sur lequel il est branché
*/ 
bool LSS05::checkIRSensor(uint8_t sensorNumber){
	return PortControl::getPinValue(port_,sensorNumber-1);
}
