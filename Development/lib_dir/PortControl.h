// Fichier : PortControl.h
// Auteur: Christopher Sigouin-Bond (1905412) | Équipes 23-28
// Date de création: 19/10/18
// But: Déclaration de la classe Port, utilisée pour simplifier
// l'utilisation des pins et des ports.

#ifndef PORTCONTROL_H
#define PORTCONTROL_H
#include <avr/io.h>

//Classe s'occupant de l'accès et de l'affectation des pins du robot.
class PortControl
{	
public:
	static void init();
	static void flipPinValue(const char port,const uint8_t pinNumber);
	static void setPinValue(const char port,const uint8_t pinNumber, const bool value);
	static bool getPinValue(const char port,const uint8_t pinNumber);
};
#endif
