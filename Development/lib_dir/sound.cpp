/****************************************************
// Fichier : Sound.cpp
// Auteur: Jean-Olivier Dalphond (1873653) et Samuel Charbonneau (1897953) | Équipes 23-28
// Date de création: 12/11/18
// Description: Implémentation de la classe Sound
*****************************************************/

#include "sound.h"
#include "Utils.h"
#include <stdio.h>

// Initialisation
void Sound::init()
{
	cli();
	// division d'horloge par 64 - implique une frequence de PWM fixe
    TCCR0A |= (1<<COM0A0)|(1<<WGM01);
    TCCR0B |= (1<<CS01)|(1<<CS00);
    sei();
}

// méthode pour arrêter le son
void Sound::stop()
{
    OCR0A=0;
}

// pour faire jouer une note sans la lumière
void Sound::playMusicNoLight(NoteRythme notes[])
{	
	// size_t size = sizeof(notes)/sizeof(notes[0]); <--- ???? pourquoi ça marche pas??
	for (int i=0; i < 22; i++)
	{
		OCR0A = getFreq(notes[i].note);
		Utils::wait(notes[i].delay);
	}
}


// pour faire jouer une note avec la lumière ambrée
void Sound::playMusic(NoteRythme notes[])
{	
	// size_t size = sizeof(notes)/sizeof(notes[0]); <--- ???? pourquoi ça marche pas??
	for (int i=0; i < 22; i++)
	{
		OCR0A = getFreq(notes[i].note);
		Utils::waitAmber(notes[i].delay);
	}
}

// pour faire jouer une chanson avec la lumière Ambrée
void Sound::playSongNoLight()
{
	init();
	NoteRythme music[] = {
		{53,250},
		{0,250},
		{51,250},
		{0,250},
		{49,250},
		{0,250},
		{48,250},
		{0,250},
		{48,125},
		{49,75},
		{0,50},
		{49,75},
		{0,50},
		{49,125},
		{0,250},
		{0,250},
		{48,125},
		{49,75},
		{0,50},
		{49,75},
		{0,50},
		{49,125}/*,
		{0,250},
		{0,250},
		{58,250},
		{0,250},
		{53,167},
		{52,167},
		{53,166},
		{54,250},
		{54,250},
		{53,250},
		{0,250},
		{0,250},
		{0,250},
		{57,250},
		{0,250},
		{58,250}*/
	};
	playMusicNoLight(music);
	stop();
}

// pour faire jouer une chanson avec la lumière Ambrée
void Sound::playSong()
{
	init();
	NoteRythme music[] = {
		{53,250},
		{0,250},
		{51,250},
		{0,250},
		{49,250},
		{0,250},
		{48,250},
		{0,250},
		{48,125},
		{49,75},
		{0,50},
		{49,75},
		{0,50},
		{49,125},
		{0,250},
		{0,250},
		{48,125},
		{49,75},
		{0,50},
		{49,75},
		{0,50},
		{49,125}/*,
		{0,250},
		{0,250},
		{58,250},
		{0,250},
		{53,167},
		{52,167},
		{53,166},
		{54,250},
		{54,250},
		{53,250},
		{0,250},
		{0,250},
		{0,250},
		{57,250},
		{0,250},
		{58,250}*/
	};
	playMusic(music);
	stop();
}

// 8000000/(frequence*2*256)
// 15625/freq = valeur à passer au OCR0A, retournée par le switch case
double Sound::getFreq(uint8_t note){
	switch(note){
		case 0:
			return 0;
			break;
		case 45:
			// 110 Hz (A)
			return 142.045045;
			break;
		case 46:
			// 116.5409404 Hz (A#)
			return 134.073055755;
			break;
		case 47:
			// 123.4708253 Hz (B)
			return 126.548113387;
			break;
		case 48:
			// 130.8127827 Hz (C)
			return 119.445513485;
			break;
		case 49:
			// 138.5913155 Hz (C#)
			return 112.741551977;
			break;
		case 50:
			// 146.832384 Hz (D)
			return 106.413854862;
			break;
		case 51:
			// 155.5634919 Hz (D#)
			return 100.441304121;
			break;
		case 52:
			// 164.8137785 Hz (E)
			return 94.80396689;
			break;
		case 53:
			// 174.6141157 Hz (F)
			return 89.48302912;
			break;
		case 54:
			// 184.9972114 Hz (F#)
			return 84.460732579;
			break;
		case 55:
			// 195.997718 Hz (G)
			return 79.720315927;
			break;
		case 56:
			// 207.6523488 Hz (G#)
			return 75.245958403;
			break;
		case 57:
			// 220 Hz (A)
			return 71.022727273;
			break;
		case 58:
			// 233.0818808 Hz (A#)
			return 67.036527878;
			break;
		case 59:
			// 246.9416506 Hz (B)
			return 63.274056693;
			break;
		case 60:
			// 261.6255653 Hz (C)
			return 59.722756765;
			break;
		case 61:
			// 277.182631 Hz (C#)
			return 56.370775988;
			break;
		case 62:
			// 293.6647679 Hz (D)
			return 53.206927449;
			break;
		case 63:
			// 311.1269837 Hz (D#)
			return 50.220652076;
			break;
		case 64:
			// 329.6275569 Hz (E)
			return 47.40198346;
			break;
		case 65:
			// 349.2282314 (F)
			return 44.74151456;
			break;
		case 66:
			// 369.9944227 (F#)
			return 42.230366301;
			break;
		case 67:
			// 391.995436 (G)
			return 39.860157964;
			break;
		case 68:
			// 415.3046976 Hz (G#)
			return 37.622979201;
			break;
		case 69:
			// 440 Hz (A)
			return 35.511363636;
			break;
		case 70:
			// 466.1637615 Hz (A#)
			return 33.518263946;
			break;
		case 71:
			// 493.88233013 Hz (B)
			return 31.637090551;
			break;
		case 72:
			// 523.2511306 Hz (C)
			return 29.861378383;
			break;
		case 73:
			// 554.365262 Hz (C#)
			return 28.185387994;
			break;
		case 74:
			// 587.3295358 Hz (D)
			return 26.603463725;
			break;
		case 75:
			// 622.2539674 Hz (D#)
			return 25.110326038;
			break;
		case 76:
			// 659.2551138 Hz (E)
			return 23.70099173;
			break;
		case 77:
			// 698.4564629 Hz (F)
			return 22.370757277;
			break;
		case 78:
			// 739.9888454 Hz (F#)
			return 21.115183151;
			break;
		case 79:
			// 783.990872 Hz (G)
			return 19.930078982;
			break;
		case 80:
			// 830.6093952 Hz (G#)
			return 18.811489601;
			break;
		case 81:
			// 880 Hz (A)
			return 17.755681818;
	}
}
