/****************************************************
// Fichier : deplacement.h
// Auteur: Samuel Charbonneau  | Équipes 23-28
// Date de création: 14/11/18
// Description: Gestion du déplacement du robot via le capteur de ligne LSS05
*****************************************************/
#ifndef MOVEMENT_H
#define MOVEMENT_H
#define F_CPU 8000000UL


#include "LSS05.h"
#include "motor.h"
#include <avr/io.h>
#include <util/delay.h>

enum class States{INIT, START, START_TO_A, A_TO_B, B_TO_C, C_TO_D, D_TO_E, E_TO_F, F_TO_END,END};

class Movement{
	
	public:
		Movement();
		
		/* Fonction qui esécute le parcours*/
		uint8_t run();
		
		/* fonction pour tester les differents fonctionnalité*/
		void test();
	
		/*arrête le déplacement du robot*/
		void pause();
	
	private:
		/*Fonctions selon l'état courrant*/
		void init();
		void start();
		void start_to_a();
		void a_to_b();
		void b_to_c();
		void c_to_d();
		void d_to_e();
		void e_to_f();
		void f_to_end();
		void end();
		
		int dtoi(Direction dir);
		
		/* Fonction qui s'occupera de suivre la ligne */
		void followLineForward();
		/* Fonction qui s'occupera de suivre la ligne */
		void followLineBackward();
		
		/*Vérifier qu'on est bel et bien au bout de la droite*/
		bool testStillOnLine();
		
		/*repositionne le centre du robot sur la ligne*/
		bool repositionningRobot(Direction side);
		
		/*Change le robot de line pour la ligne horizontale*/
		bool changeLane(Direction side);
		
		/*Pour donne un petit boost lorsqu'on active à nouveau les moteurs*/
		void giveBoost(Direction side,Direction dir);
		
		/*Classe des capteurs IR pour les ligne*/
		LSS05 capteurs;
		
		/*pour savoir si le robot est déjà en déplacement*/
		bool isMoving;
		
		/* pour stocké l'état courant*/
		States currentState;
		
		/*Pour savoir lorsqu'on est sur un segment*/
		bool lineStarted;
		
		
		bool firstTime;
		
		
		
		/* Compteur Temporaire a enlever */ 
		void startCounter();
		void count();
		void countDown();
		int counter;
		
		bool finished;
	
	
	
	
	
};





#endif
