/*
 * Nom: WallRenderer.cpp
 * Auteur: Sebastien Zerbato - �quipe 23-28
 * Date: 13 novembre 2018
 */
#include "WallRenderer.h"

WallRenderer::WallRenderer()
{
    
}

uint8_t WallRenderer::quickMedian(uint8_t *uncertainValues)
{
    uint8_t i;
    uint8_t j;
    uint8_t x=MIDDLE_INDEX;

    do
    {
        i=0x00;
        j=LAST_INDEX;
        while(uncertainValues[i]<=uncertainValues[x]&&i!=x)i++;
        while(uncertainValues[j]>=uncertainValues[x]&&j!=x)j--;
        /*[i] <-> [j]*/
        uint8_t t=uncertainValues[i]; 
        uncertainValues[i]=uncertainValues[j];
        uncertainValues[j]=t;
    }
    while(i!=j);

    return uncertainValues[x];

}

void WallRenderer::centerFromRight(Point& data)
{
    data.r+=DISTANCE_CORRECTION;
    data.l-=DISTANCE_CORRECTION;
}

void WallRenderer::centerFromLeft(Point& data)
{
    data.r-=DISTANCE_CORRECTION;
    data.l+=DISTANCE_CORRECTION;
}

Point WallRenderer::calculateDistances()
{
    float deltaY = (float) TOTAL_DISTANCE / (float) (distancesCurrentIndex - 1);

    float deltaXR, deltaXL;
    float totR = 0;
    float totL = 0;
    float e = -0.178; // incertitude d'acquisition des mesures

    for(unsigned int i = 0; i < distancesCurrentIndex - 1; i++)
    {
        deltaXL = abs(distances[i+1].l - distances[i].l);
        deltaXR = abs(distances[i+1].r - distances[i].r);

        totL += pythagore(deltaXL, deltaY) + e;
        totR += pythagore(deltaXR, deltaY) + e;
    }

    totL = round(totL);
    totR = round(totR);

    wallLength.l = totL;
    wallLength.r = totR;

    return wallLength;
}

Point WallRenderer::acq(bool dir)
{
    Point t;
    initRendering(t);
       
    if(dir)
        centerFromRight(t);
    else
        centerFromLeft(t);

    if(acqCounter_ == ACQ_PERIOD)
    {
        if( distancesCurrentIndex < NUMBER_OF_MEDIAN_OBSERVATION )
        {
            distances[distancesCurrentIndex++] = t;
        }
    }

    acqCounter_ = (acqCounter_ + 1) % (ACQ_PERIOD + 1);
    return t;
}

void WallRenderer::initRendering(Point& t)
{
    /*fill temp array*/
    for(uint8_t i = 0; i <= LAST_INDEX; i++)
    { 
        valuesL_[i] = wsm_.getDistanceFromLeftWall();
        valuesR_[i] = wsm_.getDistanceFromRightWall();
    }

    t.l=quickMedian(valuesL_);
    t.r=quickMedian(valuesR_);
}

void WallRenderer::print()
{
    wd_.displayWalls(distances, distancesCurrentIndex);
}

float WallRenderer::pythagore(float a, float o)
{
    return sqrt((a*a) + (o*o));
}
