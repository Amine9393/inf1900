/****************************************************
// Fichier : Sound.h
// Auteur: Jean-Olivier Dalphond (1873653) et Samuel Charbonneau (1897953) | Équipes 23-28
// Date de création: 12/11/18
// Description: Entête de la classe Sound
*****************************************************/

#ifndef SOUND_H
#define SOUND_H

#include <avr/io.h>
#include <avr/interrupt.h>
#include "Utils.h"

// chaque note a une première valeur pour la hauteur du son (tableau midi)
// et une deuxième pour le rythme (en ms pour le moment)
typedef struct
{
	uint8_t note;
	uint8_t delay;
}
NoteRythme;

class Sound{
public:
	// initialisation
	static void init();
	// méthode pour arrêter le son
	static void stop();
	// méthode pour générer une note (entre 45 et 81) avec un rythme (en ms)
    static void playMusicNoLight(NoteRythme notes[]);
	// méthode pour générer une note (entre 45 et 81) avec un rythme (en ms)
    static void playMusic(NoteRythme notes[]);
    // méthode pour faire jouer la chanson en une ligne de code avec lumière Ambrée
    static void playSongNoLight();
     // méthode pour faire jouer la chanson en une ligne de code sans lumière
    static void playSong();
   
private:
    // méthode privée pour traduire le numéro de la note en valeur à passer au timer
    static double getFreq(uint8_t note);
};

#endif
