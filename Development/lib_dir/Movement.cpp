/****************************************************
// Fichier : Movement.h
// Auteur: Samuel Charbonneau  | Équipes 23-28
// Date de création: 14/11/18
// Description: Gestion du déplacement du robot via le capteur de ligne LSS05
*****************************************************/

#include "Movement.h"


/** ----------------- Section Public ----------------- **/


/*
 * Constructeur.
 * Initialise les attributs.
*/
Movement::Movement(){
	isMoving=false;
	currentState=States::INIT;
	lineStarted=false;
	firstTime=true;
}
/* fonction pour tester les differents fonctionnalité*/
void Movement::test(){
	followLineBackward();
}

/*
 * Fonction qui fait le parcours
 * Retourne l'information qui va dire au robot s'il peut capter des murs ou non, 
 * s'il est dans un changement de ligne ou à la fin.
*/
uint8_t Movement::run(){

	switch(currentState){
		case States::INIT:
			init();
			return 0;
			
		case States::START:
			start();
			return 2;

		case States::START_TO_A:
			start_to_a();
			return 2;

		case States::A_TO_B:
			a_to_b();
			return 0;

		case States::B_TO_C:
			b_to_c();
			return 0;

		case States::C_TO_D:
			c_to_d();
			return 2;

		case States::D_TO_E:
			d_to_e();
			return 0;

		case States::E_TO_F:
			e_to_f();
			return 0;

		case States::F_TO_END:
			f_to_end();
			return 2;

		case States::END:
			end();
			return 1;
		
	};
}

/*arrête le déplacement du robot*/
void Movement::pause(){
	Motor::stop();
	isMoving=false;
	_delay_ms(1200);
}



/** ----------------- Section Privé ----------------- **/


/*
 * Fonction qui s'occupe de démarer le robot et de le faire avancer vers le Départ M1.
 * Le robot suivra la ligne jusqu'au point A.
*/
void Movement::init(){
	followLineForward();
	if(capteurs.checkIfAllSensorDetectSame()==detectAllSame::LINE){
		currentState=States::START;
	}
}
/*
 * Fonction qui s'occupe d'avancer au dessus du point A.
 * Le robot Avancer tout droit pendant ce cours laps de temps.
*/
void Movement::start(){
	isMoving=true;
	if(!lineStarted){
		Motor::straight(dtoi(Direction::FORWARD));
		if(!capteurs.detectCorner(Direction::RIGHT))
			lineStarted=true;
	}else{
		currentState=States::START_TO_A;
		lineStarted=false;
	}
}


/*
 * Du départ au point a. Prendra des données pendant ce temps.
 */ 
void Movement::start_to_a(){
	followLineForward();
	
	if(capteurs.detectCorner(Direction::RIGHT)){
		currentState=States::A_TO_B;
		firstTime=true;
	}
	
}


/*
 * Du point a au point b. Ne prendra de données pendant ce temps.
 * Si le robot ne détecte plus de ligne sous lui, il vérifie s'il est au bout de la ligne avec un petit tweak.
 */ 
void Movement::a_to_b(){
	/*if(firstTime)
	{
		pause();
		firstTime=false;
	}*/
	followLineForward();
	
	if(capteurs.checkIfAllSensorDetectSame()==detectAllSame::NOTHING){
		if(!testStillOnLine()){
			pause();
			currentState=States::B_TO_C;
		}
	}
	
}

/*
 * Du point b au point c. Ne prendra de données pendant ce temps.
 * Se fait en trois étapes
 * 1) Allez sur la ligne AC
 * 2) Avancer sur cette ligne jusqu'à ce que le centre de rotation soit sur le point c.
 * 3) Tourner sur sois même pour pointer vers le point D
 * 4) reculer pour que les capteurs soit tout juste derrière le point c
 */ 
void Movement::b_to_c(){
	//1)
	isMoving=false;
	while(changeLane(Direction::RIGHT)){/*Do Nothing*/};
	pause();
	
	//2)
	/*Avancer jusqu'au centre de rotation*/
	startCounter();
	while(!capteurs.detectCorner(Direction::LEFT)){
		followLineForward();
		count();
	}
	while(capteurs.checkIfAllSensorDetectSame()!=detectAllSame::NOTHING){
		Motor::straight(dtoi(Direction::FORWARD));
		count();
	}
	pause();
	giveBoost(Direction::CENTERED,Direction::FORWARD);
	//Pour que les roues s'enlignent parfaitement sur la droite CD
		Motor::straight(dtoi(Direction::FORWARD));
		_delay_ms(50);
		
	while(counter>0){
		countDown();
		Motor::straight(dtoi(Direction::FORWARD));
	}
	pause();
	
	//3)
	while(repositionningRobot(Direction::LEFT)){
	};
	pause();
	//4)
	
	while(!capteurs.detectCornerByTwo(Direction::LEFT)){
		followLineBackward();
	}
		currentState=States::C_TO_D;
		pause();
	lineStarted=false;	
}


/*
 * Du point c au point d. Prendra des données pendant ce temps.
 */ 
void Movement::c_to_d(){
	if(!lineStarted){
		if(!isMoving){
			giveBoost(Direction::CENTERED,Direction::FORWARD);
			isMoving=true;
		}
		Motor::straight(dtoi(Direction::FORWARD));
		if(capteurs.endOfLine(Direction::LEFT))
			lineStarted=true;
	}else{
		followLineForward();
		if(capteurs.detectCorner(Direction::LEFT)){
			currentState=States::D_TO_E;
			firstTime=true;
			lineStarted=false;
		}
	}
	
}

/*
 * Du point d au point e. Ne prendra de données pendant ce temps.
 * Si le robot ne détecte plus de ligne sous lui, il vérifie s'il est au bout de la ligne avec un petit tweak.
 */ 
void Movement::d_to_e(){if(firstTime=true)
	/*if(firstTime)
	{
		pause();
		firstTime=false;
	}*/
	followLineForward();
	
	if(capteurs.checkIfAllSensorDetectSame()==detectAllSame::NOTHING){
		if(!testStillOnLine()){
			pause();
			currentState=States::E_TO_F;
		}
	}
	
}


/*
 * Du point e au point f. Ne prendra de données pendant ce temps.
 * Se fait en trois étapes
 * 1) Allez sur la ligne DF
 * 2) Avancer sur cette ligne jusqu'à ce que le centre de rotation soit sur le point F.
 * 3) Tourner sur sois même pour pointer vers la fin
 * 4) reculer pour que les capteurs soit tout juste derrière le point F
 */ 
void Movement::e_to_f(){
	//1)
	giveBoost(Direction::CENTERED,Direction::BACKWARD);
	Motor::turnLeft();
	_delay_ms(700);
	while(changeLane(Direction::LEFT)){/*Do Nothing*/};
	pause();
	
	//2)
	/*Avancer jusqu'au centre de rotation*/
	startCounter();
	while(!capteurs.detectCorner(Direction::RIGHT)){
		followLineForward();
		count();
	}
	while(capteurs.checkIfAllSensorDetectSame()!=detectAllSame::NOTHING){
		Motor::straight(dtoi(Direction::FORWARD));
		count();
	}
	pause();
	giveBoost(Direction::CENTERED,Direction::FORWARD);
		Motor::straight(dtoi(Direction::FORWARD));
	while(counter>0){
		countDown();
		Motor::straight(dtoi(Direction::FORWARD));
	}
	pause();
	
	//3)
	while(repositionningRobot(Direction::RIGHT)){
	};
	pause();
	//4)
	while(!capteurs.detectCornerByTwo(Direction::RIGHT)){
		followLineBackward();
	}
		currentState=States::F_TO_END;
		pause();
	lineStarted=false;	
}

/*
 * Du point f à la fin. Prendra des données pendant ce temps.
 */ 
void Movement::f_to_end(){
	if(!lineStarted){
		if(!isMoving){
			giveBoost(Direction::CENTERED,Direction::FORWARD);
			isMoving=true;
		}
		Motor::straight(dtoi(Direction::FORWARD));
		if(capteurs.endOfLine(Direction::LEFT))
			lineStarted=true;
	}else{
		followLineForward();
		if(capteurs.checkIfAllSensorDetectSame()==detectAllSame::LINE){
			currentState=States::END;
			Motor::stop();
			lineStarted=false;
		}
	}
}

void Movement::end(){
}


	
	
	

/*
 * Fonction qui s'occupe de suivre la ligne lorsque le robot avance.
 *  
*/
void Movement::followLineForward(){
	if(!isMoving){
		giveBoost(Direction::CENTERED,Direction::FORWARD);
		isMoving=true;
	}
	switch(capteurs.getOrientation()){
		case Direction::RIGHT:
			Motor::turnSlightyLeft(dtoi(Direction::FORWARD));
			break;
		case Direction::LEFT:
			Motor::turnSlightyRight(dtoi(Direction::FORWARD));
			break;
		case Direction::CENTERED:
			Motor::straight(dtoi(Direction::FORWARD));
			break;
	};
}

/*
 * Fonction qui s'occupe de suivre la ligne lorsque le robot recule.
 *  
*/
void Movement::followLineBackward(){
	if(!isMoving){
		giveBoost(Direction::CENTERED,Direction::BACKWARD);
		isMoving=true;
	}
	switch(capteurs.getOrientation()){
		case Direction::RIGHT:
			Motor::turnSlightyRight(dtoi(Direction::BACKWARD));
			break;
		case Direction::LEFT:
			Motor::turnSlightyLeft(dtoi(Direction::BACKWARD));
			break;
		case Direction::CENTERED:
			Motor::straight(dtoi(Direction::BACKWARD));
			break;
	};
}

/*
 * Vérifier qu'on est bel et bien au bout de la droite.
 * Va bouger un peu vers la gauche pour savoir s'il détecte une ligne.
*/
bool Movement::testStillOnLine(){
	pause();
	if(!isMoving){
		giveBoost(Direction::LEFT,Direction::FORWARD);
		isMoving=true;
	}
	startCounter();
	bool stillOnLine=false;
	Direction sensorSide;
	while(!stillOnLine && counter<500){
		stillOnLine = capteurs.atLeastOneSensorDetect();
		sensorSide = capteurs.wichSensorSide();
		Motor::turnLeft_90();
		count();
	};
	pause();
	
	startCounter();
	if(sensorSide==Direction::LEFT && stillOnLine){
	    while(repositionningRobot(Direction::LEFT)&&counter<300 ){
		    count();
	    };
    }
    else if(stillOnLine){
	    while(repositionningRobot(Direction::RIGHT )&&counter<300){
		    count();
	    };
    }
    else{
		startCounter();
			giveBoost(Direction::RIGHT,Direction::FORWARD);
		 while(counter<500){
			Motor::turnRight_90();
		    count();
	    };
	}
			
	pause();
		
		
	return stillOnLine;	
}
		

/*
 * Va repositionner le centre du robot sur la ligne.
 * Il tournera sur lui même
*/
bool Movement::repositionningRobot(Direction side){
	if(capteurs.isCentered()){
		pause();
		return false;
	}else{
		if(!isMoving){
			giveBoost(side,Direction::FORWARD);
			isMoving=true;
		}
		if(side==Direction::RIGHT)
			Motor::turnRight_90();
		else
			Motor::turnLeft_90();
		return true;
	}	
	
}	

/*
 * Pour mettre le robot sur la ligne horizontale
 * Il tournera sur un pivot qui est en fait une de ses roues selon le sens de rotation
*/

bool Movement::changeLane(Direction side){
	if(capteurs.isCentered()){
		pause();
		return false;
	}else{
		if(!isMoving){
			giveBoost(side,Direction::BACKWARD);
			isMoving=true;
		}
		if(side==Direction::RIGHT)
			Motor::turnRight();
		else
			Motor::turnLeft();
		return true;
	}	
}

/*
 * Donne un petit boost de moteur juste pour donner un momentum au robot pour qu'il puisse avancer à basse vitesse par la suite.
*/
void Movement::giveBoost(Direction side,Direction dir){
	switch (side){
		case Direction::CENTERED:
			Motor::straight(dtoi(dir),255);
			break;
		case Direction::LEFT:
			Motor::turnLeft_90(255);
			break;
		case Direction::RIGHT:
			Motor::turnRight_90(255);
			break;
	};
	
	_delay_ms(25);
}


/*
 * Pour compter
*/

void Movement::startCounter(){
	counter=0;
}
void Movement::count(){
	counter++;
	_delay_ms(1);
}

void Movement::countDown(){
	counter--;
	_delay_ms(1);
}


int Movement::dtoi(Direction dir)
{
	switch(dir)
	{
		case Direction::FORWARD:
			return 0;
		
		case Direction::BACKWARD:
			return 1;
	}
}
