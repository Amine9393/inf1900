

#include "Light.h"

void Light::setLight(Colors color)
{
    bool value1 = false; 
    bool value2 = false;

    //Getting the right values for each color
    switch(color)
    {
        case Colors::RED:
            value1 = true;
        break;

        case Colors::GREEN:
            value2 = true;
        break;

        case Colors::NONE:
        break;
    }

    PortControl::setPinValue(DEL.port, DEL.pin1, value1);
    PortControl::setPinValue(DEL.port, DEL.pin2, value2);
}

void Light::blink(uint8_t times, Colors color1, Colors color2)
{
    for(uint8_t i = 0; i < times; i++)
    {
        setLight(color1);
        _delay_ms(1000);
        setLight(color2);
        _delay_ms(1000);
    }  
}

void Light::toggle(Colors color1, Colors color2)
{
    if(getCurrentColor() == color1)
    {
        setLight(color2);
    }
    else if(getCurrentColor() == color2)
    {
        setLight(color1);
    }
    else
    {
        setLight(color1);
    }
}

Colors Light::getCurrentColor()
{
    bool val1, val2 = false;

    val1 = PortControl::getPinValue(DEL.port, DEL.pin1);
    val2 = PortControl::getPinValue(DEL.port, DEL.pin2);

    if(val1 == val2)
    {
        return Colors::NONE;
    }
    else
    {
        if(val1)
            return Colors::RED;
        else
            return Colors::GREEN;
    }
}
