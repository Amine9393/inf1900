/* Fichier: distanceDisplayer.cpp
 * Auteur: Christopher Sigouin-Bond, équipe 23-28
 * Date: 20/11/18
 */ 
#include "wallDisplay.h"

 // Constructeur
WallDisplay::WallDisplay() { }

// Affiche les distances stockées dans un tableau.
// distances [in] distances entre les murs gauche et droit et le robot.
void WallDisplay::displayWalls(const Point distances[], int size)
{
	uint8_t l = 0;
	uint8_t r = 0;
	uint8_t ratio = size / LINES_TO_WRITE;
	float rem = ( (float) size / (float) LINES_TO_WRITE ) - (float) ratio;
	float currentRem = 0;
	
	uint8_t lineCount = 0;
	uint8_t avg = 0;

	for(int i = size - 1; i >= 0; i--)
	{
		if (lineCount >= 96)
			break;
					
		l += distances[i].l;
		r += distances[i].r;
		avg++;
		
		if (currentRem >= 1 && i > 0)
		{
			currentRem -= 1;
			i--;
			l += distances[i].l;
			r += distances[i].r;
			avg++;
		}
		
		if((i % ratio) == 0)
		{
			l /= avg;
			r /= avg;

			printLine(l, r);
			lineCount++;

			l = 0;
			r = 0;
			avg = 0;
		}
		
		currentRem += rem;
	}
}

// Affiche une ligne de l'affichage des murs.
// distances [in] distances entre les murs gauche et droit et le
// 				  millieu du parcours
void WallDisplay::printLine(uint8_t l, uint8_t r)
{
	Light::toggle(Colors::GREEN);
	uint8_t charL =  CHARACTERS_HALF - ((float) l / (float) TUNNEL_HALF_WIDTH) * CHARACTERS_HALF;
	uint8_t charR =  CHARACTERS_HALF - ((float) r / (float) TUNNEL_HALF_WIDTH) * CHARACTERS_HALF;

	for(int i = 0; i < CHARACTERS_HALF; i++)
	{
		if(i < charL)
		{
			Communication::send('@');
		}
		else
		{
			Communication::send(' ');
		}
	}

	Communication::send('|');
	Light::toggle(Colors::GREEN);
	Communication::send('|');

	for(int i = CHARACTERS_HALF - 1; i >= 0; i--)
	{
		if(i < charR)
		{
			Communication::send('@');
		}
		else
		{
			Communication::send(' ');
		}
	}

	Communication::send('\n');
}
