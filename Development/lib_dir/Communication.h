#ifndef COMMUNICATION_H
#define COMMUNICATION_H

#include <avr/io.h>
#include <avr/interrupt.h>

class Communication
{
public:
	static void init();
	static void send(uint8_t data);
	static uint8_t receive(void);
};

#endif /* COMMUNICATION_H */
