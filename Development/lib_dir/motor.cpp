#include "motor.h"
/*
Moteur droit:
    power: D6
    sens: D4
Moteur gauche:
    power: D5
    sens: D3
pour avancer, sens = 0;
pour reculer, sens = 1
    */
    
    /*ajustementPWM1(Droit, Gauche)*/


void Motor::straight(int dir,uint8_t speed ){

	//Nous avons un moteur qui fonction plus rapidement que l'autre donc avec 
	//l'ajustement de 20 le robot en ligne droite
    if(speed > 250)
        PWM::ajustementPWM1(speed,speed);
    else
        PWM::ajustementPWM1(speed,speed + 7);
    /*Set pin to go forward*/
    /*Gauche*/ 
    PortControl::setPinValue('D',2,dir);
    /*Droit*/ 
    PortControl::setPinValue('D',3,dir);
}

void Motor::turnRight_90(uint8_t speed){
	//Nous avons un moteur qui fonction plus rapidement que l'autre donc avec 
	//l'ajustement de 7 le robot tourne mieux
    PWM::ajustementPWM1(speed,speed);
    /*Set pin to go forward*/
    /*Gauche*/ 
    PortControl::setPinValue('D',2,0);
    /*Droit*/ 
    PortControl::setPinValue('D',3,1);
}
void Motor::turnLeft_90(uint8_t speed){
    PWM::ajustementPWM1(speed,speed);
    /*Set pin to go forward*/
    /*Gauche*/ 
    PortControl::setPinValue('D',2,1);
    /*Droit*/ 
    PortControl::setPinValue('D',3,0);
}

void Motor::turnRight(uint8_t speed){
	//Nous avons un moteur qui fonction plus rapidement que l'autre donc avec 
	//l'ajustement de 7 le robot tourne mieux
    PWM::ajustementPWM1(speed,0);
    /*Set pin to go forward*/
    /*Gauche*/ 
    PortControl::setPinValue('D',2,1);
    /*Droit*/ 
    PortControl::setPinValue('D',3,1);
}
void Motor::turnLeft(uint8_t speed){
    PWM::ajustementPWM1(0,speed);
    /*Set pin to go forward*/
    /*Gauche*/ 
    PortControl::setPinValue('D',2,1);
    /*Droit*/ 
    PortControl::setPinValue('D',3,1);
}


void Motor::turnSlightyRight(int dir,uint8_t speed ){

    if(speed > 250)
        PWM::ajustementPWM1(2*speed/3,speed);
    else
        PWM::ajustementPWM1(2*speed/3,speed + 7);

    /*Set pin to go forward*/
    /*Gauche*/ 
    PortControl::setPinValue('D',2,dir);
    /*Droit*/ 
    PortControl::setPinValue('D',3,dir);
}
void Motor::turnSlightyLeft(int dir,uint8_t speed ){

    PWM::ajustementPWM1(speed,2*(speed + 7)/3);
    /*Set pin to go forward*/
    /*Gauche*/ 
    PortControl::setPinValue('D',2,dir);
    /*Droit*/ 
    PortControl::setPinValue('D',3,dir);
}

void Motor::stop(){
	PWM::ajustementPWM1(0,0);
}
