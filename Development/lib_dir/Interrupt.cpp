/****************************************************
// Fichier : Interrupt.cpp
// Auteur: Amine Kamal (1718831) | Équipes 23-28
// Date de création: 24/11/18
// Description: Définition de la classe Interrupt
*****************************************************/

#include "Interrupt.h"

//Fonction qui attend l'evenement "Click" 
// "Click" -> bouton pressé et relaché
//rien ne peut se dérouler durant l'attente
void Interrupt::waitClicked(char port, uint8_t pinNumber)
{
	waitPressed(port, pinNumber);
	waitReleased(port, pinNumber);
}

//Fonction qui attend l'evenement "Press" (appui sur le bouton seulement)
// rien ne peut se dérouler durant l'attente
void Interrupt::waitPressed(char port, uint8_t pinNumber)
{
	while(!checkPin(port, pinNumber));
}

//Fonction qui attend l'evenement "Release" (relachement du bouton seulement)
// rien ne peut se dérouler durant l'attente
// A utiliser seulement apres avoir attendu un wait pressed sinon la fonction n'attendera pas
void Interrupt::waitReleased(char port, uint8_t pinNumber)
{
	while(checkPin(port, pinNumber));
}

//Fonction qui vérifie l'état actuel d'une pin avec un anti rebond
// Renvoi une valeur booleene qui correspond a letat de la pin  
bool Interrupt::checkPin(char port, uint8_t pinNumber)
{
	if(PortControl::getPinValue(port, pinNumber))
	{
		_delay_ms(10);
		if(PortControl::getPinValue(port, pinNumber))
			return true;
	}

	return false;
}