/****************************************************
// Fichier : Interrupt.h
// Auteur: Amine Kamal (1718831) | Équipes 23-28
// Date de création: 24/11/18
// Description: Entête de la classe Interrupt
*****************************************************/

#ifndef INTERRUPT_H
#define INTERRUPT_H

#include "PortControl.h"
#include <util/delay.h>

//Using scrutation => can't do other things while waiting
class Interrupt
{
public:
	static void waitClicked(const char port,const uint8_t pinNumber);
	static void waitPressed(const char port,const uint8_t pinNumber);
	static void waitReleased(const char port,const uint8_t pinNumber);

private:
	static bool checkPin(char port, uint8_t pinNumber);
};

#endif
