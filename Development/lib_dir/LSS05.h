/****************************************************
// Fichier : LSS05.h
// Auteur: Samuel Charbonneau  | Équipes 23-28
// Date de création: 14/11/18
// Description: Gestion des capteurs du suiveur de ligne LSS05
*****************************************************/
#ifndef LSS05_H
#define LSS05_H
#define F_CPU 8000000UL


#include "PortControl.h"
#include "Globals.h"
#include <util/delay.h>

/*
 * Pour pouvoir vérifier l'orientation du robot
*/
/*
 * Pour pouvoir vérifier si le robot détecte la même chose sur tous ses capteurs.
*/
enum class detectAllSame{NONE,LINE,NOTHING};

enum class Direction
{
	FORWARD = 0,
	BACKWARD = 1,
	LEFT = 2,
	RIGHT = 3,
	CENTERED = 4
};

class LSS05{
	public: 
		/*Constructeur*/
		LSS05();
	
		
		
		/* Vérifie l'orientation du robot */
		Direction getOrientation();
		
		/*Vérifie juste si le robot est centré*/
		bool isCentered();
		
		/* Vérifie si le robot détecte soit une ligne horizontale ou aucune ligne*/
		detectAllSame checkIfAllSensorDetectSame();
		
		/* Vérifie si le robot détecte un coin*/
		bool detectCorner(Direction sideToCheck);
		
		/* Vérifie si le robot détecte un coin avec que deux capteurs*/
		bool detectCornerByTwo(Direction sideToCheck);
		
		bool endOfLine(Direction sideToCheck);
		
		/* Vérifie s'il un capteur detect quelque chose*/
		bool atLeastOneSensorDetect();
		
		/* Vérifie quel côté les capteurs détectent quelque chose*/
		Direction wichSensorSide();
		
		/*Pour calibré les capteurs du robot*/
		void calibrate();
		
	private:
	
		/*Vérifient l'état d'un capteur*/
		 bool checkIRSensor(uint8_t sensorNumber);
		
	
		char port_;
		Direction currentOrientation_;

		
};
#endif
