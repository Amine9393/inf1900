/****************************************************
// Fichier : Utils.cpp
// Auteur: Amine Kamal (1718831) | Équipes 23-28
// Date de création: 30/10/18
// Description: Définition de la classe Utils
*****************************************************/

#include "Utils.h"

void Utils::wait(unsigned int value)
{
	for(int i = 0; i < value; i++)
		_delay_ms(1);
}

void Utils::waitAmber(unsigned int value)
{
	for(uint8_t i = 0; i < (value/2); i++)
	{
		Light::setLight(Colors::RED);
		_delay_ms(1);
		Light::setLight(Colors::GREEN);
		_delay_ms(1);
	}
}
