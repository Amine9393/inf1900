/*
 * Nom: WallSensorManager.cpp
 * Auteur: Christopher Sigouin-Bond - équipe 23-28
 * Date: 13 novembre 2018
 */

#include "wallSensorManager.h"
// Obtient les données captées par le capteur de mur gauche.
// return les données brutes de la distance entre le robot et le mur gauche
uint8_t WallSensorManager::getLeftSensorData()
{
	return getSensorData(LEFT_SENSOR_PIN);
}

// Obtient les données captées par le capteur de mur droit.
// return les données brutes de la distance entre le robot et le mur droit
uint8_t WallSensorManager::getRightSensorData()
{
	return getSensorData(RIGHT_SENSOR_PIN);
}

// Obtient les données captées par un capteur de mur.
// pinNumber [in] Le numéro de la pin du port A prenant les données
// en entrée
// return  les données bruts de la distance entre le robot et le mur.
uint8_t WallSensorManager::getSensorData(const uint8_t pinNumber)
{
	return uint8_t(converter_.lecture(pinNumber)>>2);
}

// Calcule la distance entre le centre du robot et un mur.
// data: donnée obtenue par un capteur de mur.
// return: la distance en centimètre.
uint8_t WallSensorManager::calculateDistanceCm(const uint8_t data ) const
{
	// Formule obtenue par regression de données expérimentales.
	// R² = 0.996
	return uint8_t(917 * pow(data, -0.843));
}

// Permet d'obtenir la distance entre le mur gauche et le robot.
// return la distance en centimètres
uint8_t WallSensorManager::getDistanceFromLeftWall()
{
	uint8_t data = getLeftSensorData();
	return calculateDistanceCm(data);
	
	/*float ret;
	if(data >= 63)
	{
		ret = round(-20.16 * log((double) data) + 117.16);
	}
	else
	{
		ret = round(-47.91 * log((double) data) + 231.33);
	}
	
	return (uint8_t) ret; */
}

// Permet d'obtenir la distance entre le mur droit et le robot.
// return la distance en centimètres
uint8_t WallSensorManager::getDistanceFromRightWall()
{
	uint8_t data = getRightSensorData();
	return calculateDistanceCm(data);
	
	/*float ret;
	if(data >= 71)
	{
		ret = round(-22.53 * log((double) data) + 128.83);
	}
	else
	{
		ret = round(-49.55 * log((double) data) + 243.59);
	}
	
	return (uint8_t) ret;*/
}

