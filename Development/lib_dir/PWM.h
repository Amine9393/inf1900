/****************************************************
// Fichier : PWM.h
// Auteur: Sébastien Zerbato (1573817) | Équipes 23-28
// Date de création: 23/10/18
// Description: Entête de la classe PWM
*****************************************************/
#ifndef PWM_H
#define PWM_H

#include <avr/io.h>
#include <avr/interrupt.h>

class PWM{
    public: 
       static void ajustementPWM1(uint8_t a1, uint8_t b1);
       static void ajustementPWM2(uint8_t a2, uint8_t b2);
};

#endif
