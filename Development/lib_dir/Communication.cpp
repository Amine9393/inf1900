/****************************************************
// Fichier : Communication.cpp
// Auteur: Amine Kamal (1718831) | Équipes 23-28
// Date de création: 19/10/18
// Description: Définition de la classe Communication
*****************************************************/

#include "Communication.h"

void Communication::init()
{
	cli();
	// 2400 bauds. Nous vous donnons la valeur des deux
	// premier registres pour vous éviter des complications
	UBRR0H = 0;
	UBRR0L = 0xCF;

	// permettre la reception et la transmission par le UART0
	UCSR0B = (1 << RXEN0) | (1 << TXEN0);

	// Format des trames: 8 bits, 1 stop bits, no parity
	UCSR0C = 0 
		  | (0 << UPM01) | (0 << UPM00)
		  | (0 << USBS0)
		  | (1 << UCSZ01) | (1 << UCSZ00);
	sei();
}

void Communication::send(uint8_t data) 
{
	while (!( UCSR0A & (1<<UDRE0))); // wait while register is free
    UDR0 = data; 
}

uint8_t Communication::receive(void)
{
	while(!( UCSR0A & (1<<RXC0)));
 	return UDR0;
}
