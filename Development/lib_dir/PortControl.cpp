// Fichier : PortControl.cpp
// Auteur: Christopher Sigouin-Bond (1905412) | �quipes 23-28
// Date de cr�ation: 19/10/18
// But: Impl�mentation de la classe PortControl.


#include "PortControl.h"

void PortControl::init()
{
	DDRA = 0x0; // PORT A est en mode entree
	DDRB = 0x0f; // PORT B est en mode sortie
	DDRC = 0xff; // PORT C est en mode sortie
	DDRD = 0xff; // PORT D est en mode entree
}

void PortControl::flipPinValue(const char port,const uint8_t pinNumber)
{
	uint8_t changeToApply = 0x00 | (1 << pinNumber);	
	switch (port){
		case 'A':
			PORTA ^= changeToApply;
			break;
		case 'B':
			PORTB ^= changeToApply;
			break;
		case 'C':
			PORTC ^= changeToApply;
			break;
		case 'D':
			PORTD ^= changeToApply;
			break;
		default: //Entr�e invalide
			break;
	}
}

// Affecte une valeur � une pin de sortie.
// [in] port: port de la pin � inverser
// [in] pinNumber: num�ro de la pin � inverser
void PortControl::setPinValue(const char port,const uint8_t pinNumber, 
							  const bool value)
{
	uint8_t changeToApply;
	if (value == false)
	{
		changeToApply = ~(0x00| (!value << pinNumber));
		
		switch (port){
			case 'A':
				PORTA &= changeToApply;
				break;
			case 'B':
				PORTB &= changeToApply;
				break;
			case 'C':
				PORTC &= changeToApply;
				break;
			case 'D':
				PORTD &= changeToApply;
				break;
			default: //Entr�e invalide
				break;
		}
	}
	else
	{
		changeToApply = (value << pinNumber);
		switch (port){
		case 'A':
			PORTA |= changeToApply;
			break;
		case 'B':
			PORTB |= changeToApply;
			break;
		case 'C':
			PORTC |= changeToApply;
			break;
		case 'D':
			PORTD |= changeToApply;
			break;
		default: //Entr�e invalide
			break;
		}
	}
}


// Lit le signal d'une pin d'entr�e.
// [in] pin Indique la pin � lire.
// return Valeur de la pin.
bool PortControl::getPinValue(const char port,const uint8_t pinNumber)
{
	uint8_t bitToRead = (1 << pinNumber);
	
	switch (port)
	{
		case 'A':
			return (PINA & bitToRead);
		case 'B':
			return (PINB & bitToRead);
		case 'C':
			return (PINC & bitToRead);
		case 'D':
			return (PIND & bitToRead);
		default: // Entr�e non-valide
			break;
	}
}
