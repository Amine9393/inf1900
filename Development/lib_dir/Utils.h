/****************************************************
// Fichier : Utils.h
// Auteur: Amine Kamal (1718831) | Équipes 23-28
// Date de création: 30/10/18
// Description: Entête de la classe Utils
*****************************************************/

#ifndef UTILS_H
#define UTILS_H

#define F_CPU 8000000UL //8 MHz
#include <util/delay.h>
#include "Light.h"

class Utils
{
public:
	static void wait(unsigned int value);
	static void waitAmber(unsigned int value);
};

#endif
