/****************************************************
// Fichier : motor.h
// Auteur: Samuel Charbonneau et Jean-Olivier Dalphond  | Équipes 23-28
// Date de création: 30/10/18
// Description: Gestion des moteurs
*****************************************************/
#ifndef MOTOR_H
#define MOTOR_H
#define F_CPU 8000000UL

#include "PWM.h"
#include "PortControl.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

class Motor{
    public:
    static void straight(int dir,uint8_t speed=85 );
    static void turnRight_90(uint8_t speed=86);
    static void turnLeft_90(uint8_t speed=86);
    static void turnRight(uint8_t speed=90);
    static void turnLeft(uint8_t speed=90);
    static void turnSlightyRight(int dir,uint8_t speed=87 );
    static void turnSlightyLeft(int dir,uint8_t speed=87 );
    static void stop();

    private:
    
};


#endif
