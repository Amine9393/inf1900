Disco Ball
Par Christopher Sigouin-Bond, Samuel Charbonneau, Amine Kamal, Jean-Olivier Dalphond, Sebastien Zerbato

Table des matières

1. Classes 
	1.1 Communication
	1.2 Light
	1.3 Interrupt
	1.4 LCD
	1.5 LSS05
	1.6 Motor
	1.7 Movement
	1.8 PWM
	1.9 PortControl
	1.10 Robot
	1.11 Sound
	1.12 Utils
	1.13 WallDisplay
	1.14 WallRenderer
	1.15 WallSensorManager

2. Autres fichiers:
	2.1 main.cpp

3. Comment mettre en marche le robot
	3.1 Prérequis
	3.2 Branchement du robot
	3.3 Mise en marche

4. Fichiers de code qui nous ont été fournis
	4.1 can
	4.2 memoire_24
	4.3 customprocs, lcm_so1602dtr_m et lcm_so1602dtr_m_fw

5. Remerciements


_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_
 | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | |


1. Classes

		Dans cette section, le mode d'emploi des classes sera présenté.

		Pour certaines d'entre elles, il peut être souhaitable d'en changer le code
		si on le désire; la méthode pour le faire sera décrite dans ces classes
		elles-mêmes.


	1.1 Communication
		Cette classe permet de simplifier l'envoi et la réception de données via I2C
		en mettant à profit des méthodes plus difficiles d'utilisation de memoire_24.
		
	1.2 Light
		Cette classe est utilisée pour allumer, éteindre ou faire clignoter la DEL du
		robot. Il est possible d'indiquer la couleur voulue et la classe inclut une
		struct qui gère le port et les PINs selon le branchement de la DEL.
		
	1.3 Interrupt
		Nous avons créé une classe qui gère les interruptions générées par le bouton
		du robot. Celle-ci communique avec la classe PortControl pour la gestion des
		ports (entrée/sortie).
		
	1.4 LCD
		Cette classe agit comme interface entre la classe LCM déjà fournie et
		les autres classes de notre robot. Nous voulions ajouter une couche
		d'abstraction pour gérer de façon plus transparente les différentes
		composantes du robot. Il faut simplement instancier un objet de la
		classe LCD pour appeler ses différentes méthodes.
		
	1.5 LSS05
		Cette classe s'occupe de lire les informations du suiveur de ligne.
		Il y a plusieurs moyen de lire le capteur. On peut par exemple vérifier
		l'orientation du robot, vérifier si nous avons atteint un coin, une
		ligne horizontale ou même rien du tout. LSS05 est en fait le nom du
		capteur que nous avons sur le robot.

	1.6 Motor
		La classe Motor permet d'appeler des fonctions de déplacement de base, que le
		robot doit faire pour compléter les différentes étapes du parcours. C'est donc
		une classe qui fait le pont entre la classe PWM et Movement.
		
	1.7 Movement
		Cette classe s'occupe de faire le déplacement haut niveau du robot sur le parcours. 
		En fait, elle utilise une machine à état pour savoir où le robot se situe
		sur le parcours. Ainsi, la classe n'a d'autre choix que de communiquer avec
		la classe LSS05 pour bien diriger le robot sur le parcours. De plus, pour
		que le robot se déplace physiquement, elle doit indiquer aux moteurs comment
		faire actionner les roues. 
		
	1.8 PWM
		La classe PWM a pour rôle de générer un PWM (Pulse Width Modulation), que nous
		utilisons pour les moteurs notamment. Nous avons créé deux méthodes pour contrôler
		le PWM sur le Timer1 et le PWM sur le Timer2.
	
	1.9 PortControl
		La classe PortControl rend la lecture et l'écriture des données sur les broches
		du robot simple avec les méthode publique de la classe.

		Si vous souhaitez utiliser cette classe pour un robot qui utilise des port différents, 
		il faut changer les méthodes de la classe en copiant un bloc de code pour un port ou 
		une broche supplémentaire, et peut-être d'utiliser un entier non-signé sur plus de bits
		si le robot a plus de 8 broches par port.

		De plus, pour changer le mode (entrée ou sortie) d'une broche, il faut charger les bits
		dans la méthode init, en sachant qu'une pin à 0 est en mode entrée, et à 1, en sortie.

	1.10 Robot
		Cette classe est la mère de toutes les autres, en quelque sorte. C'est via cette
		classe que nous sommes en mesure de contrôler toutes les autres pour compléter le
		parcours.
		
		Il y a des "states" pour chaque étape, et c'est cette classe qui gère les changements
		d'états. C'est également via cette classe que nous pouvons gérer l'envoi des données
		à la fin du parcours.
	
	1.11 Sound
		Afin de gérer l'émission de sons et de la musique par le robot, nous
		avons créé une classe Sound, qui fait appel au Timer0 du microcontrôleur
		et, pour la musique à la fin du parcours, également à la classe Light
		pour afficher la lumière ambrée, comme requis. Il est possible de
		générer des notes selon le même principe qu'avec le protocole MIDI.

	1.12 Utils
		Contient des fonctions utilitaires pour le programme comme wait, qui génère un délai
		grâce à la fonction _delay_ms().
		
	1.13 WallDisplay
		WallDisplay s'occupe de prendre les distances de WallRenderer et de les convertir dans
		un format acceptable pour l'affichage des distance en applicatiquant une mise à l'échelle
		sur le tableau de données pour réduire le nombre de données, qui sera aussi fait par
		WallDisplay.
		
		Pour changer les paramètres d'affichage, il suffit de changer les constantes définies dans
		le corps de la classe.
		
	1.14 WallRenderer
		Cette classe a pour but d'enregistrer les données captées par WallSensorManager, 
		de gérer les incertitudes, qu'elles soient liées aux variations de déplacement lorsque 
		le robot se repositionne, ou quand elles proviennent des incertitudes inhérentes 
		aux capteurs de distance. Finalement, cette classe permet le calcul des longueurs
		des murs captés lors du parcours.
		
		La gestion des incertitudes se fait à l'aide d'une fonction quickMedian, basée sur
		l'algorithme développée par Tony Hoare, l'auteur de l'algorithme populaire quickSort.
		Cet algorithme a pour but de tronquer les valeures sortant de la norme, on procède 
		à l'aide de deux "buffers", qui permettent d'avoir une valeure plus certaine.
		
		Le calcul des longueurs des murs se fait par le biais d'une méthode dénomée
		"Fast inverse square root" que l'on a modifiée afin qu'elle calcule directement la racine.
		
		
		La méthode précedente permet de calculer la longueur du mur entre deux points à l'aide 
		du théorème de Pythagore.
		
	1.15 WallSensorManager
		Cette classe s'occupe de capturer la tension produite par les capteur de murs, pour
		ensuite calculer la distance entre le centre du robot et les murs gauche et droit.
		
		La formule du calcul de la distance peut changer dépendamment de la tension utilisée
		pour alimenter le robot. Il est donc nécessaire de prendre quelques mesures pour
		faire une regression afin de trouver une fonction qui donne de bons résultats à chaque
		fois que l'on change de source de tension.

_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_
 | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | |

2. Autres fichiers:
		
	2.1 main.cpp
		Fichier contenant la fonction main du programme.
		
_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_
 | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | |
 
3. Comment mettre en marche le robot

	3.1 Prérequis
		L'installation du logiciel requiert un ordinateur avec GCC, make
		AVRDude, gcc-avr, avr-libc et toutes les dépendances qui en
		découlent. Il faut également un câble USB type B.

	3.2 Branchement des composantes
		Les capteurs de distance doivent être placés sur le côté et
		branchés sur le port A, le LCD sur le port C, le Piezo et les
		roues sur le port D. Pour plus d'informations, veuillez
		consulter les fichiers des classes pour lesquelles vous voulez
		obtenir plus d'information.

	3.3 Mise en marche
		Une fois le robot connecté, il suffit d'exécuter la commande:
								$ make install
		et le placer sur la ligne de départ du parcours.

_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_
 | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | |

4. Fichiers de code qui nous ont été fournis
	4.1 can
		Cette classe permet de convertir une tension produite par un capteur en un signal numérique
		sur un uint16_t.
		
	4.2 memoire_24
		Les méthodes de cette classe d'occupent de changer les valeurs des registres utilisées pour
		l'écriture et la lecture en I2C.
		
	4.3 customprocs, lcm_so1602dtr_m et lcm_so1602dtr_m_fw
		La classe LCM utilise la surcharge de l'opérateur << pour afficher directement certains types
		de données sur l'écran LCD.
_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_
 | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | |

5. Remerciements
	Merci à Jérôme Collin, Laurent Tremblay,
	Anes Mohammed Belfodil, Philippe Kavalec, Bruno Bousquet, Nathalie Deviana Bekaert, Yoan Gauthier